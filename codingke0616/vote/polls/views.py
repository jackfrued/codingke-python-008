from urllib.parse import unquote

from django.db import DatabaseError
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import render, redirect

from polls.captcha import Captcha
from polls.models import Subject, Teacher, User
from polls.utils import gen_random_code, gen_md5_digest


def show_subjects(request: HttpRequest) -> HttpResponse:
    subjects = Subject.objects.all().order_by('no')
    return render(request, 'subjects.html', {
        'subjects': subjects
    })


def show_teachers(request: HttpRequest) -> HttpResponse:
    try:
        sno = int(request.GET.get('sno'))
        teachers = []
        if sno:
            subject = Subject.objects.only('name').get(no=sno)
            teachers = Teacher.objects.filter(subject=subject).order_by('no')
        return render(request, 'teachers.html', {
            'subject': subject,
            'teachers': teachers
        })
    except (ValueError, Subject.DoesNotExist):
        return redirect('/')


def praise_or_criticize(request: HttpRequest) -> HttpResponse:
    if request.session.get('userid'):
        try:
            tno = int(request.GET.get('tno'))
            teacher = Teacher.objects.get(no=tno)
            if request.path.startswith('/praise/'):
                teacher.good_count += 1
                count = teacher.good_count
            else:
                teacher.bad_count += 1
                count = teacher.bad_count
            teacher.save()
            data = {'code': 20000, 'mesg': '投票成功', 'count': count}
        except (ValueError, Teacher.DoesNotExist):
            data = {'code': 20001, 'mesg': '投票失败'}
    else:
        data = {'code': 20002, 'mesg': '请先登录'}
    return JsonResponse(data)


def get_captcha(request: HttpRequest) -> HttpResponse:
    captcha_text = gen_random_code()
    request.session['captcha'] = captcha_text.lower()
    image_data = Captcha.instance().generate(captcha_text)
    return HttpResponse(image_data, content_type='image/png')


def login(request: HttpRequest) -> HttpResponse:
    hint, backurl = '', request.GET.get('backurl', '/')
    if request.method == 'POST':
        backurl = request.POST.get('backurl', '/')
        if backurl != '/':
            backurl = unquote(backurl)
        captcha_from_serv = request.session.get('captcha', '0')
        captcha_from_user = request.POST.get('captcha', '1').lower()
        if captcha_from_serv == captcha_from_user:
            username = request.POST.get('username')
            password = request.POST.get('password')
            if username and password:
                password = gen_md5_digest(password)
                user = User.objects.filter(username=username, password=password).first()
                if user:
                    request.session['userid'] = user.no
                    request.session['username'] = user.username
                    return redirect(backurl)
                else:
                    hint = '用户名或密码错误'
            else:
                hint = '请输入有效的用户名和密码'
        else:
            hint = '请输入有效的验证码'
    return render(request, 'login.html', {
        'hint': hint, 'backurl': backurl
    })


def logout(request: HttpRequest) -> HttpResponse:
    request.session.flush()
    return redirect('/')


def register(request: HttpRequest) -> HttpResponse:
    hint = ''
    if request.method == 'POST':
        agreement = request.POST.get('agreement')
        if agreement:
            username = request.POST.get('username')
            password = request.POST.get('password')
            tel = request.POST.get('tel')
            if username and password and tel:
                try:
                    password = gen_md5_digest(password)
                    user = User(username=username, password=password, tel=tel)
                    user.save()
                    return redirect('/login/')
                except DatabaseError:
                    hint = '注册失败，请尝试更换用户名'
            else:
                hint = '请输入有效的注册信息'
        else:
            hint = '请勾选同意网站用户协议及隐私政策'
    return render(request, 'register.html', {'hint': hint})
