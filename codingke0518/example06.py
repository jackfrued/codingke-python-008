"""
从文件中读取列表/字典
"""
import json


def main():
    with open('files/fruits.txt', 'r', encoding='utf-8') as file:
        fruits = json.load(file)
        print(type(fruits), fruits)
    with open('files/person.txt', 'r', encoding='utf-8') as file:
        person = json.load(file)
        print(type(person), person)


if __name__ == '__main__':
    main()
