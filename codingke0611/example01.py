"""
作用域问题：Local ---> Embedded ---> Global ---> Built-in
          局部        嵌套          全局         内置
global / nonlocal
"""
a = 100


def foo():
    def bar():
        global a
        a = 300
        print(a)

    bar()
    print(a)


foo()
print(a)
