"""
JSON ---> JavaScript Object Notation ---> JavaScript创建对象的字面量语法 ---> 数据格式

JSON: {name: '骆昊', age: 40, sex: true}
Python字典：{'name': '骆昊', 'age': 40, 'sex': True}

网络API（数据接口）---> 免费的/付费的 ---> 获取程序中需要的数据 ---> JSON
    ~ 生活服务类：天气、空气质量、疫情、物流、垃圾分类
    ~ 金融类：实名认证、银行卡认证、商标、油价、黄金、股票
    ~ 新闻类：头条、体育新闻、时事新闻、热点事件
    ~ 娱乐类：明星八卦、美女图片、彩虹屁
    ~ 知识类：诗词、成语、十万个为什么、百科
    ~ 其他类：聊天机器人、图像识别、验证码破解
Python中要实现并发编程有三种方式：
    1. 多线程
    2. 多进程
    3. 异步编程
"""
# import json
import time

from threading import Thread

import ujson
import requests


def download(pic_url, filename):
    resp = requests.get(pic_url)
    with open(f'images/{filename}', 'wb') as file:
        file.write(resp.content)


def main():
    resp = requests.get('http://api.tianapi.com/meinv/index?key=83b8591aded2d2c7a2cca3d682d1d860&rand=1&num=30')
    print(type(resp.text), resp.text)
    data_dict = ujson.loads(resp.text)
    items = data_dict['newslist']
    start = time.time()
    threads = []
    for item in items:
        pic_url = item['picUrl']
        filename = pic_url[pic_url.rfind('/') + 1:]
        print(pic_url, filename)
        # target - 线程启动之后要执行的任务，通常是一个函数或对象方法
        # args - 线程启动后要执行的任务的参数
        t = Thread(target=download, args=(pic_url, filename))
        threads.append(t)
        # 调用start方法启动线程
        t.start()
    # 等待所有线程结束
    for t in threads:
        # 等待t线程对象执行结束
        t.join()
    end = time.time()
    print(f'下载图片的总时间: {end - start:.3f}秒')


if __name__ == '__main__':
    main()
