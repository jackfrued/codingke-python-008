## 扣丁学堂 - Python全栈开发 - 第008期

### 2020年04月27日

1. 授课主题：《列表类型的应用》
2. 授课内容：
   - 列表元素的重复
   - 列表的索引运算
   - 列表的循环遍历
   - 列表的生成式语法
   - 列表元素的随机抽样
   - 列表元素的排序
   - 列表元素的添加和删除
   - 嵌套列表的创建和应用
3. 相关代码：[codingke0427](./codingke0427)

### 2020年04月29日

1. 授课主题：《函数的定义和使用》
2. 授课内容：
   - 什么是函数
   - 为什么需要函数
   - 定义函数的语法
   - 调用函数的语法
   - 带默认值的参数
   - 函数返回多个值
3. 相关代码：[codingke0429](./codingke0429)
4. 作业参考：[《0429-函数的应用作业参考答案》](./作业/0429-函数的定义和使用作业参考答案.md)
5. 作业评讲：第8题、第9题、第12题

### 2020年05月06日~2020年05月08日

1. 授课主题：《函数使用进阶》
2. 授课内容：
   - 位置参数、关键字参数、可变参数
   - 用模块管理函数
   - Python内置函数
   - Python内置模块
   - 高阶函数
   - Lambda函数
   - 函数的递归调用

3. 相关代码：[codingke0506](./codingke0506)
4. 作业参考：[《0506-函数使用进阶作业参考答案》](作业/0506-函数使用进阶作业参考答案.md)

### 2020年05月10日

1. 授课主题：《集合和字典类型的应用》
2. 授课内容：
   - 集合的定义
   - 集合与列表的区别
   - 集合的运算和方法
   - 字典的作用和定义
   - 字典的运算和方法
   - 字典的应用
3. 相关代码：[codingke0510](./codingke0510)
4. 作业参考：[《0510-集合和字典类型的应用作业参考答案》](作业/0510-集合和字典类型的应用作业参考答案.md)

### 2020年05月11日

1. 授课主题：《面向对象编程入门》
2. 授课内容：
   - 面向对象的基本概念
   - 定义类和创建对象
   - 给对象发消息
   - 初始化方法
   - 用对象思维解决问题
3. 相关代码：[codingke0511](./codingke0511)

### 2020年05月13日

1. 授课主题：《面向对象编程进阶》
2. 授课内容：
   - `__repr__`魔术方法
   - 可见性和属性装饰器
   - 类方法和静态方法
   - 继承
   - 抽象类和抽象方法
3. 相关代码：[codingke0513](./codingke0513)
4. 作业参考：[《0513-面向对象编程进阶作业参考答案》](./作业/0513-面向对象编程作业参考答案.md)

### 2020年05月15日

1. 授课主题：《面向对象编程应用》
2. 授课内容：
   - 多态的应用
   - 枚举类型
   - 关联关系
   - 综合案例
3. 相关代码：[codingke0515](./codingke0515)
4. 作业参考：[《21点游戏》](./codingke0515/example04.py)

### 2020年05月18日

1. 授课主题：《文件读写和异常处理》
2. 授课内容：
    - 打开和关闭文件
    - 读写文本文件
    - 读写二进制文件
    - 异常处理
    - 字典的序列化和反序列化
3. 参考代码：[codingke0518](./codingke0518)
4. 作业参考：[《0518-文件读写和异常处理作业参考答案》](./作业/0518-文件读写和异常处理作业.md)

### 2020年05月20日

1. 授课主题：《Python中的并发编程》
2. 授课内容：
    - 线程和进程的概念
    - 创建多个线程
    - 创建多个进程
    - 使用线程池
    - 多个线程竞争资源的场景
    - 线程锁
    - 线程调度
3. 相关代码：[codingke0520](./codingke0520)

### 2020年05月25日~2020年05月26日

1. 授课主题：《Web前端知识串讲》
2. 授课内容：
    - Web前端概述
    - Web前端开发工具
    - 常用HTML标签
    - CSS选择器和样式属性
    - CSS盒模型（Box Model）
    - 用CSS实现页面布局
    - JavaScript语言基础
    - BOM（浏览器对象模型）
    - DOM（文档对象模型）
    - jQuery入门
    - Ajax技术的应用
    - Vue.js框架及其应用
3. 相关代码：[codingke0525](./codingke0525)

### 2020年05月27日~2020年05月29日

1. 授课主题：《Linux操作系统串讲》
2. 授课内容：
    - Linux云服务的购买和使用
    - Linux操作系统常用命令
    - Linux文件编辑器 - vim
    - Linux软件的安装
        - 安装Nginx
        - 安装Python 3
        - 安装MySQL
    - Linux系统管理

### 2020年05月31日~2020年06月09日

1.  授课主题：《关系型数据库MySQL》
2. 授课内容：
    - 数据库系统概述
    - 关系型数据库的概念和特点
    - MySQL的介绍和使用
    - MySQL图形化客户端工具
    - SQL语句详解
        - DDL：创建表、删除表、修改表、数据类型、约束
        - DML：插入数据、删除数据、更新数据
        - DQL：投影、别名、筛选、模糊、排序、分组、聚合、嵌套、连接
        - DCL：授予权限、召回权限
    - 视图、执行计划和索引
    - Python程序操作MySQL
3. 相关代码：[codingke0602](./codingke0602)、[codingke0606](./codingke0606)

### 2020年06月11日

1. 授课主题：《Python相关基础知识串讲》
2. 授课内容：[Python基础思维导图](Python基础思维导图.xmind)
3. 相关代码：[codingke0611](./codingke0611)

### 2020年06月13日

1. 授课主题：《Redis的安装和使用》
2. 授课内容：
    - Redis的安装和启动
    - Redis命令行工具的使用
    - Redis常用命令
    - Redis的五种核心数据类型

3. 参考资源：
    - [Redis中文网](http://redis.cn/)
    - [Redis命令参考](http://redisdoc.com/)

### 2020年06月16日

1. 授课主题：《Web应用概述和Django框架入门》
2. 授课内容：
    - 什么是Web应用
    - 用Python开发Web应用的优势
    - Django框架的介绍和使用
    - 视图函数的编写
    - 生成动态内容

### 2020年06月18日

1. 授课主题：《MVC架构和Django中的模型》
2. 授课内容：
    - 模板页的编写和使用
    - 配置数据库和解决兼容性问题
    - MVC和MTV架构
    - 通过二维表自动生成模型类
    - 模型类的介绍

### 2020年06月20日

1. 授课主题：《使用Django框架的Admin应用管理模型》
2. 授课内容：
    - 通过ORM实现CRUD操作
    - 通过模型迁移生成二维表
    - 配置和使用admin应用

### 2020年06月23日

1. 授课主题：《页面渲染和静态资源处理》
2. 授课内容：
    - 定制admin应用的显示
    - 静态资源相关的配置
    - 投票项目核心功能实现


### 2020年06月25日

1. 授课主题：《支持前端异步请求（Ajax）》
2. 授课内容：
    - 让视图函数返回JSON格式数据
    - 通过HttpRequest对象获取请求相关信息
    - Ajax技术的介绍
    - 用原生JavaScript代码实现异步请求
    - 用jQuery库实现异步请求

### 2020年06月27日

1. 授课主题：《用户跟踪和session的使用》
2. 授课内容：
    - Web应用如何实现用户跟踪
    - 浏览器实现本地存储的方式
    - 实现图片验证功能
    - 跨站请求伪造和CSRF令牌

### 2020年06月30日

1. 授课主题：《完成”投票“项目的相关功能》
2. 授课内容：
    - Cookie和session的关系
    - 完成用户登录功能
    - 判断用户是否登录
    - 实现用户注销功能
    - session相关的配置

### 2020年07月02日

1. 授课主题：《开发”车辆违章查询“项目-1》
2. 授课内容：
    - Django相关知识点复习
    - 使用Q对象实现复杂查询
    - ORM对模糊查询的支持
    - 分页查询的原理和实现

### 2020年07月04日

1. 授课主题：《开发”车辆违章查询“项目-2》
2. 授课内容：
    - 分页查询的前端实现
    - Django-Debug-Toolbar的使用
    - 解决1+N查询问题
    - 实现违章记录的受理和删除
    - 版本控制工具Git的安装


### 2020年07月07日

1. 授课主题：《导出Excel报表和生成前端图表》
2. 授课内容：
    - 生成Excel报表
    - 使用ECharts生成统计图表

### 2020年07月09日

1. 授课主题：《版本控制工具Git的使用-1》
2. 授课内容：
    - 版本控制的历史和Git介绍
    - 使用Git在本地实施版本控制
    - 使用Git服务器同步代码

### 2020年07月11日

1. 授课主题：《版本控制工具Git的使用-2》
2. 授课内容：
    - 在PyCharm中使用Git
    - 配置免密访问Git服务器
    - 创建和使用Git分支
    - 通过Pull Request合并工作成果

### 2020年07月14日

1. 授课主题：《前后端分离开发和djangorestframework》
2. 授课内容：
    - 前端渲染和前后端分离开发
    - 安装和配置djangorestframework
    - 使用FBV定制数据接口
    - 序列化器的定制和使用

### 2020年07月16日

1. 授课主题：《数据接口的实现和页面渲染》

2. 授课内容：

    - 实现项目所需的数据接口

    - 使用Vue.js实现页面渲染

### 2020年07月18日

1. 授课主题：《使用CBV定制数据接口》
2. 授课内容：
    - ModelViewSet的应用
    - 接入Redis和缓存配置
    - 实现数据接口的缓存

### 2020年07月21日

1. 授课主题：《RESTful架构和JWT的应用》
2. 授课内容：
    - RESTful架构简介
    - 基于令牌的用户跟踪
    - JWT的应用

### 2020年07月23日

1. 授课主题：《接入三方平台和任务的异步化》
2. 授课内容：
    - 接入三方平台的两种方式
    - 短信平台的介绍
    - 通过API接口调用三方服务
    - 实现阻止重复发送短信
    - 用户注册短信验证码功能

### 2020年07月25日

1. 授课主题：《企业项目开发-1》
2. 授课内容：
    - 软件项目过程模型（开发流程）
    - 使用版本控制管理项目代码
    - 项目相关准备工作

### 2020年07月28日

1. 授课主题：《企业项目开发-2》

### 2020年07月30日

1. 授课主题：《企业项目开发-3》

### 2020年08月01日

1. 授课主题：《企业项目开发-4》