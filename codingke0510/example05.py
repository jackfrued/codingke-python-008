"""
为什么需要字典类型？想一想如果要用一个变量保存一个人的信息应该怎么做？
列表好用吗？
person1 = ['王大锤', '王二锤', 55, 60, 175, '科华北路62号', '中同仁路8号', '13122334455', '13800998877']
元组好用吗？
person2 = ('王大锤', '王二锤', 55, 60, 175, '科华北路62号', '中同仁路8号', '13122334455', '13800998877')
集合好用吗？
person3 = {'王大锤', '王二锤', 55, 60, 175, '科华北路62号', '中同仁路8号', '13122334455', '13800998877'}

字典 - 用键值对的方式组织数据，通过键可以获取到对应的值，实现有的放矢的取值
"""
# 创建字典的方式一：字面量语法
person1 = {
    '姓名': '王大锤', '紧急联系人': '王二锤', '性别': True,
    '年龄': 55, 'height': 175, 'weight': 60,
    'office': '科华北路62号', 'home': '中同仁路8号',
    'tel': '13122334455', 'e_tel': '13800998877'
}
print(person1)

# 创建字典的方式二：dict函数（构造器）
person2 = dict(姓名='王大锤', age=55, 体重=60, home='中同仁路8号')
print(person2)

# zip函数 ---> 拉链 / 压缩
items1 = dict(zip('ABCDE', '12345'))
print(items1)
items2 = dict(zip('12345', 'ABCDE'))
print(items2)
items3 = dict(zip('ABCDE', range(1, 10)))
print(items3)

# 创建字典的方式三：字典生成式
items3 = {x: x ** 3 for x in range(1, 6)}
print(items3)
